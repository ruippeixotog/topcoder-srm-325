import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ModularInequalityTest {

	protected ModularInequality solution;

	@Before
	public void setUp() {
		solution = new ModularInequality();
	}

	@Test
	public void testCase0() {
		int[] A = new int[] { 1, 2, 3 };
		int P = 6;

		int expected = 5;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase1() {
		int[] A = new int[] { 10, 30, 15, -1, 17 };
		int P = 42;

		int expected = 7;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase2() {
		int[] A = new int[] { 0, 2, 3, -5, 10 };
		int P = 17;

		int expected = 0;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase3() {
		int[] A = new int[] { -693 };
		int P = 1265;

		int expected = 2531;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase4() {
		int[] A = new int[] { 965, -938, -396, -142, 926, 31, -720 };
		int P = 6495;

		int expected = 1781;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase5() {
		int[] A = new int[] { -1000000000 };
		int P = 1000000000;

		int expected = 2000000001;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testCase6() {
		int[] A = new int[] { 0 };
		int P = 1000000000;

		int expected = 2000000001;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase10() {
		int[] A = new int[] { -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000, -1000000000,
				-1000000000, -1000000000, -1000000000 };
		int P = 0;

		int expected = 1;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase72() {
		int[] A = new int[] { -10, -9 };
		int P = 18;

		int expected = 18;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testCase74() {
		int[] A = new int[] { -906331985, -595778437, -515697510, -509040636 };
		int P = 895788091;

		int expected = 397221225;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testCase82() {
		int[] A = new int[] { 1000000000 };
		int P = 1000000000;

		int expected = 2000000001;
		int actual = solution.countSolutions(A, P);

		Assert.assertEquals(expected, actual);
	}
}
