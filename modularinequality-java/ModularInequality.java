import java.util.Arrays;

public class ModularInequality {
	public int countSolutions(int[] A, int P) {
		Arrays.sort(A);
		
		long total = 0;
		for(int i = 0; i < A.length; i++) {
			total += A[i];
		}
		
		int counter = 0;
		int m = -A.length;
		long b = total;
		long y = yFor(A[0], m, b);
		counter += countLinear(-2000000000, A[0], m, b, P);
		m += 2;
		b = bFor(A[0], y, m);
		
		for(int i = 1; i < A.length; i++) {
			y = yFor(A[i], m, b);
			counter += countLinear(A[i - 1], A[i], m, b, P);
			m += 2;
			b = bFor(A[i], y, m);
		}
		counter += countLinear(A[A.length - 1], 2000000001, m, b, P);
		return counter;
	}
	
	public long bFor(int x, long y, int m) {
		return y - m * (long) x;
	}
	
	public long yFor(int x, int m, long b) {
		return m * (long) x + b;
	}
	
	public int countLinear(int x1, int x2, int m, long b, int P) {
		long y1 = m * (long) x1 + b;
		long y2 = m * (long) x2 + b;
		if(P >= Math.max(y1, y2)) {
			return x2 - x1;
		} else if(P < Math.min(y1, y2)) {
			return 0;
		} else if(y2 > y1) {
			int sect = (int) Math.floor((P - b) / (double) m);
			return sect - x1 + 1;
		} else {
			int sect = (int) Math.ceil((P - b) / (double) m);
			return x2 - sect;
		}
	}
}