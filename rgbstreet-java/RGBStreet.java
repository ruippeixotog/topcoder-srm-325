public class RGBStreet {

	public int estimateCost(String[] houses) {
		int[] lastPrices = { 0, 0, 0 };

		for (String house : houses) {
			String[] costs = house.split(" ");
			int[] nextPrices = new int[3];
			nextPrices[0] = Math.min(lastPrices[1], lastPrices[2])
					+ Integer.parseInt(costs[0]);
			nextPrices[1] = Math.min(lastPrices[0], lastPrices[2])
					+ Integer.parseInt(costs[1]);
			nextPrices[2] = Math.min(lastPrices[0], lastPrices[1])
					+ Integer.parseInt(costs[2]);
			lastPrices = nextPrices;
		}
		return Math.min(lastPrices[0], Math.min(lastPrices[1], lastPrices[2]));
	}
}
