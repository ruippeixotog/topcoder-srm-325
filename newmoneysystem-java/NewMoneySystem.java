import java.util.HashMap;
import java.util.Map;

public class NewMoneySystem {
	Map<Long, Map<Integer, Long>> minNotes = new HashMap<Long, Map<Integer, Long>>();

	public long chooseBanknotes(String N, int K) {
		long n = Long.parseLong(N);
		return minNotes(n, K - 1);
	}

	long minNotes(long n, int k) {
		if (k == 0) {
			return n;
		}
		Map<Integer, Long> withN = minNotes.get(n);
		if (withN == null) {
			withN = new HashMap<Integer, Long>();
			minNotes.put(n, withN);
		}
		Long withKN = withN.get(k);
		if (withKN == null) {
			long min = Long.MAX_VALUE;
			for (int i = 2; i <= 5; i++) {
				long value = n % i + minNotes(n / i, k - 1);
				if (value < min) {
					min = value;
				}
			}
			withN.put(k, min);
			withKN = min;
		}
		return withKN;
	}
}
