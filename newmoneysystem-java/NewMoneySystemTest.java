import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewMoneySystemTest {

    protected NewMoneySystem solution;

    @Before
    public void setUp() {
        solution = new NewMoneySystem();
    }

    @Test
    public void testCase0() {
        String N = "1025";
        int K = 6;

        long expected = 2L;
        long actual = solution.chooseBanknotes(N, K);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase1() {
        String N = "1005";
        int K = 5;

        long expected = 3L;
        long actual = solution.chooseBanknotes(N, K);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase2() {
        String N = "12000";
        int K = 14;

        long expected = 1L;
        long actual = solution.chooseBanknotes(N, K);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase3() {
        String N = "924323565426323626";
        int K = 1;

        long expected = 924323565426323626L;
        long actual = solution.chooseBanknotes(N, K);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCase4() {
        String N = "924323565426323626";
        int K = 50;

        long expected = 10L;
        long actual = solution.chooseBanknotes(N, K);

        Assert.assertEquals(expected, actual);
    }

}
