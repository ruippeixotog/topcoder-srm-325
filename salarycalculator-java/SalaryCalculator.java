public class SalaryCalculator {

	public double calcHours(int P1, int P2, int salary) {
		int p1max = P1 * 200;
		if(salary <= p1max) {
			return salary / (double) P1;
		}
		return 200.0 + (salary -  p1max) / (double) P2;
	}
}
