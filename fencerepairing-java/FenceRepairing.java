import java.util.ArrayList;
import java.util.List;

public class FenceRepairing {

	public double calculateCost(String[] boards) {
		List<Integer> vals = new ArrayList<Integer>();
		boolean first = true;
		char curr = 'X';
		int count = 0;
		for (String s: boards) {
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				if (first && c == '.') {
					continue;
				} else {
					first = false;
					if (c == curr) {
						count++;
					} else {
						vals.add(count);
						curr = c;
						count = 1;
					}
				}
			}
		}
		if (curr == 'X') {
			vals.add(count);
		}
		
		double[] prices = new double[vals.size() / 2 + 1];
		prices[0] = priceFunc(vals.get(0));

		for (int i = 2; i < vals.size(); i += 2) {
			prices[i / 2] = priceFunc(vals.get(i));
			
			for(int j = i - 2; j >= 0; j -= 2) {
				int joinLength = joinLength(vals, j, i);
				double joinPrice = priceFunc(joinLength);
				double splitPrice = splitPriceFunc(prices, j, i);
				
				if (joinPrice < splitPrice) {
					vals.set(j, joinLength);
					prices[j / 2] = joinPrice;
					while(i > j) {
						vals.remove(i--);
						vals.remove(i--);
					}
				}
			}
		}
		return splitPriceFunc(prices, 0, vals.size() - 1);
	}
	
	int joinLength(List<Integer> vals, int from, int to) {
		int length = 0;
		for (int i = from; i <= to; i++) {
			length += vals.get(i);
		}
		return length;
	}
	
	double splitPriceFunc(double[] prices, int from, int to) {
		double price = 0.0;
		for (int i = from; i <= to; i += 2) {
			price += prices[i / 2];
		}
		return price;
	}

	double priceFunc(int length) {
		return Math.sqrt(length);
	}
}
